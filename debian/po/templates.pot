#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: pipelight\n"
"Report-Msgid-Bugs-To: michael@fds-team.de\n"
"POT-Creation-Date: 2013-08-21 05:21+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-10-21 21:40+0000\n"
"X-Generator: Launchpad (build 16807)\n"
"Language: \n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Do you want to install Pipelight system-wide?"
msgstr ""

#. Type: boolean
#. Description
#: ../templates:1001
msgid "There are two ways to install Pipelight:"
msgstr ""

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"When choosing a system-wide installation (default in previous versions) this "
"will allow you and other users on this computer to use Pipelight in every "
"installed browser supporting the NPAPI plugin interface."
msgstr ""

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"When not choosing a system-wide installation you will get all files "
"installed, but still have to configure your browser manually to use "
"Pipelight. For instructions on how to do that please visit: "
"https://answers.launchpad.net/pipelight/+faq/2362"
msgstr ""

#. Type: boolean
#. Description
#: ../templates:1001
msgid "If unsure choose a systemwide installation."
msgstr ""
