#
# Enables a diagnostic mode which might be helpful to find an
# error in the configuration or installation.
# To get the error messages go to: http://fds-team.de/pipelight
#
# Information for advanced users: The diagnostic page embeds the plugin
# of type "application/x-pipelight-error" to trigger the output of some
# debug information. The plugin cannot be triggered if everything is
# working, so this only affects users with a broken installation.
# The debug output will include paths on the local filesystem and the
# linux distribution used. If you don't want to leak this information
# accidentially in case of a broken installation please either uninstall
# Pipelight or disable the diagnosticMode. [default: false]
#
diagnosticMode      = true

#
# Path to the wine directory or the wine executable. When you
# specify a directory it should contain /bin/wine.
#
winePath            = @@WINE_PATH@@

#
# Path to the wine prefix containing Silverlight
#
winePrefix          = $HOME/.wine-pipelight/

#
# The wine architecture for the wine prefix containing Silverlight
#
wineArch            = win32

#
# DLLs to overwrite in Wine
# (prevents Wine from asking for Gecko, Mono or winegstreamer)
#
wineDLLOverrides    = mscoree,mshtml,winegstreamer,winemenubuilder.exe=

#
# Path to the plugin loader executable
# (Should be set correctly by the make script)
#
pluginLoaderPath    = @@PLUGIN_LOADER_PATH@@

#
# Path to the runtime DLLs (libgcc_s_sjlj-1.dll, libspp-0.dll,
# libstdc++-6.dll). Only necessary when these DLLs are not in the same
# directory as the pluginloader executable.
#
gccRuntimeDlls      = @@GCC_RUNTIME_DLLS@@

#
# Path and name to the Silverlight directory
# You should prefer using regKey to make it easier to switch between
# different versions.
#
dllPath             = c:\Program Files\Silverlight\4.1.10329.0\
dllName             = npctrl.dll

#
# Name of the registry key at HKCU\Software\MozillaPlugins\ or
# HKLM\Software\MozillaPlugins\ where to search for the plugin path.
#
# You should use this option instead of dllPath/dllName in most cases
# since you do not need to alter dllPath on a program update.
#
# regKey            = @Microsoft.com/NpCtrl,version=1.0

#
# fakeVersion allows to fake the version string of Silverlight
# Allows to get around some version checks done by some websites
# when using an old version of Silverlight.
#
# fakeVersion       = 5.1.20513.0

#
# overwriteArg allows to overwrite/add initialization arguments
# passed by websites to Silverlight applications. You can
# use this option as often as you want to overwrite multiple
# parameters. The GPU acceleration state of Silverlight can be controlled
# by setting:
#
#   enableGPUAcceleration=false     # disable GPU acceleration
#   comment out                     # let the application decide (default)
#   enableGPUAcceleration=true      # force GPU acceleration
#
# You may need to overwrite the minimum runtime version if
# you use an old Silverlight version as some websites set
# an artificial limit for the version number although it 
# would work with older versions.
#
# overwriteArg      = minRuntimeVersion=5.0.61118.0
# overwriteArg      = enableGPUAcceleration=false
# overwriteArg      = enableGPUAcceleration=true
#

#
# windowlessmode refers to a term of the Netscape Plugin API and 
# defines a different mode of drawing and handling events.
# On some desktop enviroments you may have problems using the
# keyboard in windowless mode, on the other hand the drawing is
# more efficient when this mode is enabled. Just choose what works
# best for you. [default: false]
#
windowlessMode      = false

#
# embed defines whether the Silverlight plugin should be shown 
# inside the browser (true) or an external window (false).
# [default: true]
#
embed               = true

#
# Path to the dependency installer script provided by the compholio
# package. (optional)
#
dependencyInstaller = @@DEPENDENCY_INSTALLER@@

#
# Dependencies which should be installed for this plugin via the
# dependencyInstaller, can be used multiple times. (optional)
#
# Useful values for Silverlight are:
#
#   -> Silverlight versions (you need to adjust dllPath):
#   wine-silverlight5.1-installer
#   wine-silverlight5.0-installer
#   wine-silverlight4-installer
#
#   -> optional depependencies (required by some streaming sites)
#   wine-mpg2splt-installer
#
dependency          = wine-silverlight4-installer
dependency          = wine-mpg2splt-installer
dependency          = wine-wininet-installer

#
# Doesn't show any dialogs which require manual confirmation during
# the installation process, like EULA or DRM dialogs.
# [default: true]
#
quietInstallation   = @@QUIET_INSTALLATION@@

#
# In order to support browsers without NPAPI timer support
# (like Midori) we've implemented a fallback to
# NPN_PluginThreadAsyncCall. In the default configuration 
# a timer based approach is preferred over async calls and the
# plugin decides by itself which method to use depending on the
# browser capabilities. Setting the following option to true
# forces the plugin to use async calls. This might be mainly
# useful for testing the difference between both event handling
# approaches. [default: false]
#
# eventAsyncCall    = true

#
# The opera browser claims to provide timer functions, but they
# don't seem to work properly. When the opera detection is
# enabled Pipelight will switch to eventAsyncCall automatically
# based on the user agent string. [default: true]
#
operaDetection      = true

#
# Minimal JavaScript user agent switcher. If your page doesn't check
# the user agent before loading a Silverlight instance, you can use
# this trick to overwrite the useragent or execute any other Java-
# Script you want. You can use this command multiple times.
# Uncomment the following 4 lines for FF15 spoofing.
#
# executejavascript = var __originalNavigator = navigator;
# executejavascript = navigator = new Object();
# executejavascript = navigator.__proto__ = __originalNavigator;
# executejavascript = navigator.__defineGetter__('userAgent', function () { return 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20120427 Firefox/15.0a1'; });

#
# We are currently implementing hardware acceleration support, which
# can cause problems on some systems as especially specific open source
# drivers render only garbage when hardware acceleration is enabled.
# To prevent breaking any working solutions, we are currently
# implementing a whitelist system, which will enable hardware
# acceleration by default if the specified shell scripts returns 0.
# Otherwise we will disable it. You can still use
#
# overwriteArg      = enableGPUAcceleration=false/true
#
# to overwrite the check results. If you really want to skip this test
# you can use: silverlightGraphicDriverCheck = /bin/true
#
silverlightGraphicDriverCheck  = @@GRAPHIC_DRIVER_CHECK@@

#------------------------- EXPERIMENTAL -------------------------
# Watch out: The following section contains highly experimental
# stuff! These functions are likely not working properly yet and
# might be removed at any time.

#
# Silverlight uses a lot of timer stuff do to the window redrawing
# . In order to speed this up a bit the following switch enables
# some API hooks to do most of timer stuff in user mode (without
# having to call wine-server each time). It is still unclear
# if this option has any significant effect on the performance.
# [default: false]
#
# experimental-userModeTimer = true

#
# In order to make it possible to let a window stay opened in fullscreen, even
# if the user clicks somewhere else it is necessary to install a window class
# hook. With some plugins this could lead to other problems! [default: false]
experimental-windowClassHook = true

#
# A sandbox is a method to isolate an untrusted program from the rest of
# the system to prevent damage in case of a virus, program errors or
# similar issues. We've been developing the ability to use a (self-created)
# sandbox, but this feature still has to be considered highly experimental.
# Only use it when you REALLY know what you're doing!
#
# sandboxPath = /usr/bin/wine-sandbox