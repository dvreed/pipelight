#
# Enables a diagnostic mode which might be helpful to find an
# error in the configuration or installation.
# To get the error messages go to: http://fds-team.de/pipelight
#
# Information for advanced users: The diagnostic page embeds the plugin
# of type "application/x-pipelight-error" to trigger the output of some
# debug information. The plugin cannot be triggered if everything is
# working, so this only affects users with a broken installation.
# The debug output will include paths on the local filesystem and the
# linux distribution used. If you don't want to leak this information
# accidentially in case of a broken installation please either uninstall
# Pipelight or disable the diagnosticMode. [default: false]
#
diagnosticMode      = true

#
# Path to the wine directory or the wine executable. When you
# specify a directory it should contain /bin/wine.
#
winePath            = @@WINE_PATH@@

#
# Path to the wine prefix containing Shockwave
#
winePrefix          = $HOME/.wine-pipelight/

#
# The wine architecture for the wine prefix containing Shockwave
#
wineArch            = win32

#
# DLLs to overwrite in Wine
# (prevents Wine from asking for Gecko, Mono or winegstreamer)
#
wineDLLOverrides    = mscoree,mshtml,winegstreamer,winemenubuilder.exe=

#
# Path to the plugin loader executable
# (Should be set correctly by the make script)
#
pluginLoaderPath    = @@PLUGIN_LOADER_PATH@@

#
# Path to the runtime DLLs (libgcc_s_sjlj-1.dll, libspp-0.dll,
# libstdc++-6.dll). Only necessary when these DLLs are not in the same
# directory as the pluginloader executable.
#
gccRuntimeDlls      = @@GCC_RUNTIME_DLLS@@

#
# Path and name to the Shockwave library
# You should prefer using regKey and consider this option only if you have
# multiple versions of the Shockwave plugin installed inside the Wineprefix.
#
# dllPath           = C:\windows\system32\Adobe\Director\
# dllName           = np32dsw_1204144.dll

#
# Name of the registry key at HKCU\Software\MozillaPlugins\ or
# HKLM\Software\MozillaPlugins\ where to search for the plugin path.
#
# You should use this option instead of dllPath/dllName in most cases
# since you do not need to alter dllPath on a Shockwave update.
#
regKey              = @adobe.com/ShockwavePlayer

#
# fakeVersion allows to fake the version string of Shockwave
# We don't know any reason why it would be necessary for Shockwave
#
# fakeVersion       = 12.0.4.144

#
# overwriteArg allows to overwrite/add initialization arguments
# passed by websites to Shockwave applications. You can
# use this option as often as you want to overwrite multiple
# parameters. For a list of useful values for Shockwave take a look at
# TODO!
#
# Examples:
# TODO!
#

#
# windowlessmode refers to a term of the Netscape Plugin API and 
# defines a different mode of drawing and handling events.
# Shockwave will automatically switch to windowless mode if it is not
# disabled by the website. We set the default value to false to mime
# the behavior of a normal browser.
# [default: false]
#
windowlessMode      = false

#
# embed defines whether the Shockwave plugin should be shown 
# inside the browser (true) or an external window (false).
# [default: true]
#
embed               = true

#
# Path to the dependency installer script provided by the compholio
# package. (optional)
#
dependencyInstaller = @@DEPENDENCY_INSTALLER@@

#
# Dependencies which should be installed for this plugin via the
# dependencyInstaller, can be used multiple times. (optional)
#
# Useful values for Shockwave are:
#   wine-Shockwave-installer
#   
dependency          = wine-shockwave-installer

#
# Doesn't show any dialogs which require manual confirmation during
# the installation process, like EULA or DRM dialogs.
# [default: true]
#
quietInstallation   = @@QUIET_INSTALLATION@@

#
# In order to support browsers without NPAPI timer support
# (like Midori) we've implemented a fallback to
# NPN_PluginThreadAsyncCall. In the default configuration 
# a timer based approach is preferred over async calls and the
# plugin decides by itself which method to use depending on the
# browser capabilities. Setting the following option to true
# forces the plugin to use async calls. This might be mainly
# useful for testing the difference between both event handling
# approaches. [default: false]
#
# eventAsyncCall    = true

#
# The opera browser claims to provide timer functions, but they
# don't seem to work properly. When the opera detection is
# enabled Pipelight will switch to eventAsyncCall automatically
# based on the user agent string. [default: true]
#
operaDetection      = true

#
# Minimal JavaScript user agent switcher. If your page doesn't check
# the user agent before loading a Shockwave instance, you can use
# this trick to overwrite the useragent or execute any other Java-
# Script you want. You can use this command multiple times.
# Uncomment the following 4 lines for FF15 spoofing.
#
# executejavascript = var __originalNavigator = navigator;
# executejavascript = navigator = new Object();
# executejavascript = navigator.__proto__ = __originalNavigator;
# executejavascript = navigator.__defineGetter__('userAgent', function () { return 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20120427 Firefox/15.0a1'; });

#------------------------- EXPERIMENTAL -------------------------
# Watch out: The following section contains highly experimental
# stuff! These functions are likely not working properly yet and
# might be removed at any time.

#
# It is currently unknown whether this option affects Shockwave
# [default: false]
#
# experimental-userModeTimer = true

#
# A sandbox is a method to isolate an untrusted program from the rest of
# the system to prevent damage in case of a virus, program errors or
# similar issues. We've been developing the ability to use a (self-created)
# sandbox, but this feature still has to be considered highly experimental.
# Only use it when you REALLY know what you're doing!
#
# sandboxPath = /usr/bin/wine-sandbox