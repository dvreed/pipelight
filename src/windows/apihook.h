#ifndef ApiHook_h_
#define ApiHook_h_

extern bool installTimerHook();
extern bool handleTimerEvents();
extern bool installPopupHook();
extern bool installWindowClassHook();
extern bool installUnityHooks();

#endif // ApiHook_h_