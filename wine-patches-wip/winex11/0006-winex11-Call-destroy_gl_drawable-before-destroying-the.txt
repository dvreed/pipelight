From 477e6bd4fc7a07e819acf49b42de30dd833b8b2f Mon Sep 17 00:00:00 2001
From: Sebastian Lackner <sebastian@fds-team.de>
Date: Sat, 28 Sep 2013 04:35:17 +0200
Subject: winex11: Call destroy_gl_drawable before destroying the window

---
 dlls/winex11.drv/window.c |    3 ++-
 1 file changed, 2 insertions(+), 1 deletion(-)

diff --git a/dlls/winex11.drv/window.c b/dlls/winex11.drv/window.c
index 39d88b2..9bf8431 100644
--- a/dlls/winex11.drv/window.c
+++ b/dlls/winex11.drv/window.c
@@ -1622,6 +1622,8 @@ void CDECL X11DRV_DestroyWindow( HWND hwnd )
     struct x11drv_thread_data *thread_data = x11drv_thread_data();
     struct x11drv_win_data *data;
 
+    destroy_gl_drawable( hwnd );
+
     if (!(data = get_win_data( hwnd ))) return;
 
     destroy_whole_window( data, FALSE );
@@ -1633,7 +1635,6 @@ void CDECL X11DRV_DestroyWindow( HWND hwnd )
     XDeleteContext( gdi_display, (XID)hwnd, win_data_context );
     release_win_data( data );
     HeapFree( GetProcessHeap(), 0, data );
-    destroy_gl_drawable( hwnd );
 }
 
 
-- 
1.7.9.5

