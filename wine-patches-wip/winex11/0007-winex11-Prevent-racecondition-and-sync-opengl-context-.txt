From 459a9cd8379a4d255f3d9dfa9f4e78b258ad3ff5 Mon Sep 17 00:00:00 2001
From: Sebastian Lackner <sebastian@fds-team.de>
Date: Sat, 28 Sep 2013 15:18:43 +0200
Subject: winex11: Prevent racecondition and sync opengl context before
 swapping buffers

---
 dlls/winex11.drv/opengl.c |    3 ++-
 1 file changed, 2 insertions(+), 1 deletion(-)

diff --git a/dlls/winex11.drv/opengl.c b/dlls/winex11.drv/opengl.c
index 3e8480b..856b6e9 100644
--- a/dlls/winex11.drv/opengl.c
+++ b/dlls/winex11.drv/opengl.c
@@ -1795,12 +1795,12 @@ static BOOL glxdrv_wglMakeCurrent(HDC hdc, struct wgl_context *ctx)
         ret = pglXMakeCurrent(gdi_display, gl->drawable, ctx->ctx);
         if (ret)
         {
-            NtCurrentTeb()->glContext = ctx;
             ctx->has_been_current = TRUE;
             ctx->hdc = hdc;
             ctx->drawables[0] = gl->drawable;
             ctx->drawables[1] = gl->drawable;
             ctx->refresh_drawables = FALSE;
+            NtCurrentTeb()->glContext = ctx;
             goto done;
         }
     }
@@ -3220,6 +3220,7 @@ static BOOL glxdrv_wglSwapBuffers( HDC hdc )
         pglXSwapBuffers(gdi_display, gl->drawable);
         break;
     case DC_GL_CHILD_WIN:
+        if (ctx) sync_context( ctx );
         escape.gl_drawable = gl->drawable;
         /* fall through */
     default:
-- 
1.7.9.5

